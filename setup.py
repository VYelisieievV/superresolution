from setuptools import setup, find_packages


setup(
    name="sisrpytorch",
    version="0.0.2",
    description='DL codebase for SISR task. Affiliated with Master`s thesis at NTUU "KPI".',
    packages=find_packages(),
    author="Yelisieiev Vladyslav",
    author_email="vlad.yelisieiev@gmail.com",
    python_requires=">=3.6",
    install_requires=["numpy", "pandas", "scikit-learn", "torch", "yacs"],
)
