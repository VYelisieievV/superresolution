from pathlib import Path

from yacs.config import CfgNode as ConfigurationNode


# YACS overwrite these settings using YAML, all YAML variables MUST BE defined here
# first as this is the master list of ALL attributes.
__C = ConfigurationNode()

# importing default as a global singleton
cfg = __C
__C.DESCRIPTION = "Default config from the Singleton"

# dataset related parameters
__C.DATASET = ConfigurationNode()
__C.DATASET.NAME = "div2k"
__C.DATASET.DOWNSCALE_FACTOR = 4
__C.DATASET.HIGHRES_IMG_TYPE = "imagenet-norm"
__C.DATASET.LOWRES_IMG_TYPE = "[0, 1]"
__C.DATASET.HIGHRES_IMG_SIZE = 224
__C.DATASET.DATA_AUG_SIZE = 280

__C.DATASET.DATA_PATH = "./data/"

# augmix related parameters
__C.DATASET.DO_AUGS = False

__C.DATASET.AUGMENTATION = ConfigurationNode()
# __C.DATASET.AUGMENTATION.BLURRING_PROB = 0.25

__C.DATASET.BATCH_SIZE = 16
__C.DATASET.CPU_NUM = 4
__C.DATASET.NORMALIZE_MEAN = [0.485, 0.456, 0.406]
__C.DATASET.NORMALIZE_STD = [0.229, 0.224, 0.225]


# Model related parameters
__C.MODEL = ConfigurationNode()

__C.MODEL.PARALLEL = False
__C.MODEL.NAME = "SRGAN"

__C.MODEL.PRETRAIN_MODEL = ConfigurationNode()
__C.MODEL.PRETRAIN_MODEL.NAME = "SRResNet"

# SRGAN parameters
__C.MODEL.GENERATOR = ConfigurationNode()
__C.MODEL.GENERATOR.NAME = "ResNetGenerator"
# __C.MODEL.GENERATOR.PRETRAINED_PATH = "./artifacts/srgan/"

__C.MODEL.DISCRIMINATOR = ConfigurationNode()
__C.MODEL.DISCRIMINATOR.NAME = "SRGANDiscriminator"
# __C.MODEL.DISCRIMINATOR.PRETRAINED_PATH = "./artifacts/srgan/"

# __C.MODEL.HEAD = ConfigurationNode()
# __C.MODEL.HEAD.NAME = "simple_head"
# __C.MODEL.HEAD.ACTIVATION = "leaky_relu"
# __C.MODEL.HEAD.OUTPUT_DIMS = [168, 11, 7]
# __C.MODEL.HEAD.INPUT_DIM = 1280  # MobileNet_V2
# __C.MODEL.HEAD.HIDDEN_DIMS = [512, 256]
# __C.MODEL.HEAD.BN = True
# __C.MODEL.HEAD.DROPOUT = -1.0

__C.MODEL.SOLVER = ConfigurationNode()
# adam
__C.MODEL.SOLVER.OPTIMIZER = ConfigurationNode()

# generator
__C.MODEL.SOLVER.OPTIMIZER.GENERATOR_OPT = ConfigurationNode()
__C.MODEL.SOLVER.OPTIMIZER.GENERATOR_OPT.BASE_LR = 0.0001
__C.MODEL.SOLVER.OPTIMIZER.GENERATOR_OPT.BETAS = [0.9, 0.999]
__C.MODEL.SOLVER.OPTIMIZER.GENERATOR_OPT.NAME = "adam"
# discriminator
__C.MODEL.SOLVER.OPTIMIZER.DISCRIMINATOR_OPT = ConfigurationNode()
__C.MODEL.SOLVER.OPTIMIZER.DISCRIMINATOR_OPT.BASE_LR = 0.0001
__C.MODEL.SOLVER.OPTIMIZER.DISCRIMINATOR_OPT.BETAS = [0.9, 0.999]
__C.MODEL.SOLVER.OPTIMIZER.DISCRIMINATOR_OPT.NAME = "adam"

# sgd config
__C.MODEL.SOLVER.OPTIMIZER.SGD = ConfigurationNode()
__C.MODEL.SOLVER.OPTIMIZER.SGD.MOMENTUM = 0.9
__C.MODEL.SOLVER.OPTIMIZER.SGD.NESTEROV = False


__C.MODEL.SOLVER.PRETRAIN_GENERATOR_EPOCHS = 30
__C.MODEL.SOLVER.TOTAL_EPOCHS = 100
__C.MODEL.SOLVER.AMP = False

__C.OUTPUT_PATH = "./artifacts/deafult_experiment/"
__C.MULTI_GPU_TRAINING = False
__C.DEBUG = False


def get_cfg_defaults():
    """
    Get a yacs CfgNode object with default values
    """
    # Return a clone so that the defaults will not be altered
    # It will be subsequently overwritten with local YAML.
    return __C.clone()


def combine_cfgs(path_cfg_override: Path = None) -> ConfigurationNode:
    """
    An internal facing routine that combined CFG in the order provided.

        path_cfg_override (Path, optional):  path to path_cfg_override actual. Defaults
            to None.

    Returns:
        ConfigurationNode: cfg_base incorporating the overwrite.
    """

    if path_cfg_override is not None:
        path_cfg_override = Path(path_cfg_override)
    # Path order of precedence is:
    # Priority  1, 2 respectively
    # other CFG YAML > default.yaml

    # Load default lowest tier one:
    # Priority 2:
    cfg_base = get_cfg_defaults()

    # Merge from other cfg_path files to further reduce effort
    # Priority 1:
    if path_cfg_override is not None and path_cfg_override.exists():
        cfg_base.merge_from_file(path_cfg_override.absolute())

    return cfg_base
