import os
import cv2

import numpy as np
from torch.utils.data import Dataset, DataLoader

from yacs.config import CfgNode
from src.utils import convert_image


class SRDataset(Dataset):
    def __init__(
        self,
        data_folder,
        hr_img_type,
        lr_img_type,
        hr_img_size,
        downscale_factor=4,
        data_augmentation_size=None,
    ):
        assert (
            hr_img_size % downscale_factor == 0
        ), "Crop dimensions should be perfectly divisible with scaling factor"
        if data_augmentation_size is not None:
            assert data_augmentation_size >= hr_img_size

        self.data_folder = data_folder
        self.hr_img_size = hr_img_size
        self.downscale_factor = downscale_factor
        self.data_augmentation_size = data_augmentation_size
        self.hr_img_type = hr_img_type
        self.lr_img_type = lr_img_type

        self.images = os.listdir(data_folder)
        self.images.sort()

    def center_crop(self, img, dim):
        dim = (dim, dim)
        width, height = img.shape[1], img.shape[0]

        # process crop width and height for max available dimension
        crop_width = dim[0] if dim[0] < img.shape[1] else img.shape[1]
        crop_height = dim[1] if dim[1] < img.shape[0] else img.shape[0]
        mid_x, mid_y = int(width / 2), int(height / 2)
        cw2, ch2 = int(crop_width / 2), int(crop_height / 2)
        crop_img = img[mid_y - ch2 : mid_y + ch2, mid_x - cw2 : mid_x + cw2]
        return crop_img

    def get_random_crop(self, image, dim):
        crop_width, crop_height = dim, dim
        max_x = image.shape[1] - crop_width
        max_y = image.shape[0] - crop_height

        x = np.random.randint(0, max_x)
        y = np.random.randint(0, max_y)

        crop = image[y : y + crop_height, x : x + crop_width]

        return crop

    def __getitem__(self, i):
        img = cv2.imread(self.data_folder + self.images[i])
        if self.data_augmentation_size is not None:
            img = self.center_crop(img, self.data_augmentation_size)

            hr_img = self.get_random_crop(img, self.hr_img_size)
        else:
            hr_img = self.center_crop(img, self.hr_img_size)

        lr_size = (
            self.hr_img_size // self.downscale_factor,
            self.hr_img_size // self.downscale_factor,
        )
        lr_img = cv2.resize(hr_img, lr_size, interpolation=cv2.INTER_CUBIC)

        lr_img = lr_img.transpose(2, 0, 1)
        hr_img = hr_img.transpose(2, 0, 1)

        hr_img = convert_image(hr_img, "cv2", self.hr_img_type)
        lr_img = convert_image(lr_img, "cv2", self.lr_img_type)

        return lr_img, hr_img

    def __len__(self):
        return len(self.images)


def build_data_loader(
    data_folder: str, data_cfg: CfgNode, is_training: bool
) -> DataLoader:

    downscale_fctor = data_cfg.DOWNSCALE_FACTOR
    hres_img_type = data_cfg.HIGHRES_IMG_TYPE
    lowres_img_type = data_cfg.LOWRES_IMG_TYPE
    hres_img_size = data_cfg.HIGHRES_IMG_SIZE
    data_aug_size = data_cfg.DATA_AUG_SIZE

    dataset = SRDataset(
        data_folder,
        hres_img_type,
        lowres_img_type,
        hres_img_size,
        downscale_fctor,
        data_aug_size,
    )
    batch_size = data_cfg.BATCH_SIZE

    # limit the number of works based on CPU number.
    num_workers = min(batch_size, os.cpu_count())

    data_loader = DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=is_training,
        num_workers=num_workers,
        pin_memory=True,
    )

    return data_loader
