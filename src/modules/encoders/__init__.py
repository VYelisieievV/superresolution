from .build import ENCODER_REGISTRY, build_encoder
from src.modules.encoders.vgg19_feature_exractor import (
    build_vgg19featureextractor_encoder,
)
from src.modules.encoders.srgan_parts import (
    build_srgan_discriminator,
    build_resnet_generator,
)
