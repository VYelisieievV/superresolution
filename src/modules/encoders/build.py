from src.tools.registery import Registry
from torch import nn
from yacs.config import CfgNode

ENCODER_REGISTRY = Registry()


def build_encoder(encoder_cfg: CfgNode, *args, **kwargs) -> nn.Module:
    """
    Create encoder block with dynamic change of modules by changing config .yaml file.

    Args:
        encoder_cfg (CfgNode): Config file.

    Returns:
        nn.Module: initialized encoder.
    """
    encoder = ENCODER_REGISTRY[encoder_cfg.NAME](encoder_cfg, *args, **kwargs)
    return encoder
