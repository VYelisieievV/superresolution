import torch
from torch import nn
from yacs.config import CfgNode
import math

from . import ENCODER_REGISTRY
from src.modules.layers import ConvolutionalBlock, ResidualBlock, UpsamplingBlock


class ResNetGenerator(nn.Module):
    def __init__(
        self,
        n_channels=64,
        large_kernel_size=9,
        small_kernel_size=3,
        n_blocks=16,
        scaling_factor=4,
    ):
        super(ResNetGenerator, self).__init__()

        self.conv1 = ConvolutionalBlock(
            in_channels=3,
            out_channels=n_channels,
            kernel_size=large_kernel_size,
            batch_norm=False,
            activation="PReLU",
        )

        self.residual_blocks = nn.Sequential(
            *[
                ResidualBlock(n_channels=n_channels, kernel_size=small_kernel_size)
                for _ in range(n_blocks)
            ]
        )

        self.conv2 = ConvolutionalBlock(
            in_channels=n_channels,
            out_channels=n_channels,
            kernel_size=small_kernel_size,
            batch_norm=True,
        )

        n_upsampling_blocks = int(math.log2(scaling_factor))
        self.upsampling_blocks = nn.Sequential(
            *[
                UpsamplingBlock(
                    n_channels=n_channels,
                    kernel_size=small_kernel_size,
                    upscaling_factor=2,
                )
                for _ in range(n_upsampling_blocks)
            ]
        )

        self.conv3 = ConvolutionalBlock(
            in_channels=n_channels,
            out_channels=3,
            kernel_size=large_kernel_size,
            batch_norm=False,
            activation="Tanh",
        )

    def forward(self, lr_img):
        output = self.conv1(lr_img)
        residual = output
        output = self.residual_blocks(output)
        output = self.conv2(output)
        output = output + residual
        output = self.upsampling_blocks(output)
        output = self.conv3(output)

        return output


class SRGANDiscriminator(nn.Module):
    def __init__(self, kernel_size=3, n_blocks=8, full_size=1024):
        super(SRGANDiscriminator, self).__init__()

        # Container for all convocational layers
        conv_blocks = list()

        # Number of input channels
        in_channels = 3
        # Number of output channels
        out_channels = 64

        # Append all the convocational layers
        for i in range(n_blocks):
            stride = 1
            if i % 2 == 1:
                stride = 2

            batch_norm = i != 0

            conv_blocks.append(
                ConvolutionalBlock(
                    in_channels=in_channels,
                    out_channels=out_channels,
                    kernel_size=kernel_size,
                    stride=stride,
                    batch_norm=batch_norm,
                    activation="LeakyReLU",
                )
            )

            in_channels = out_channels

            if i % 2 == 1:
                out_channels *= 2

        self.conv_blocks = nn.Sequential(*conv_blocks)
        self.adaptive_avgpool = nn.AdaptiveAvgPool2d((14, 14))
        self.linear1 = nn.Linear(in_channels * 14 * 14, full_size)
        self.leaky_relu = nn.LeakyReLU(0.2)
        self.linear2 = nn.Linear(full_size, 1)
        self.sigmoid = nn.Sigmoid()

    def forward(self, input):
        batch_size = input.size(0)

        output = self.conv_blocks(input)
        output = self.adaptive_avgpool(output)
        output = self.linear1(output.view(batch_size, -1))
        output = self.leaky_relu(output)
        output = self.linear2(output)
        output = self.sigmoid(output)

        return output


@ENCODER_REGISTRY.register("ResNetGenerator")
def build_resnet_generator(encoder_cfg: CfgNode, *args, **kwargs) -> nn.Module:
    """
    Helper function to build ResNetGenerator as described in SRGAN.

    Args:
        encoder_cfg (CfgNode): Config object from .yaml file.

    Returns:
        [nn.Module]: Initialized ResNetGenerator.
    """

    model = ResNetGenerator(*args, **kwargs)
    if encoder_cfg.get("pretrained_path"):
        pretrained_path = encoder_cfg["pretrained_path"]
        state_dict = torch.load(pretrained_path, map_location="cpu")
        model.load_state_dict(state_dict, strict=False)
    return model


@ENCODER_REGISTRY.register("SRGANDiscriminator")
def build_srgan_discriminator(encoder_cfg: CfgNode, *args, **kwargs) -> nn.Module:
    """
    Helper function to build SRGANDiscriminator as described in SRGAN.

    Args:
        encoder_cfg (CfgNode): Config object from .yaml file.

    Returns:
        [nn.Module]: Initialized SRGANDiscriminator.
    """

    model = SRGANDiscriminator(*args, **kwargs)
    if encoder_cfg.get("pretrained_path"):
        pretrained_path = encoder_cfg["pretrained_path"]
        state_dict = torch.load(pretrained_path, map_location="cpu")
        model.load_state_dict(state_dict, strict=False)
    return model
