import torch
from torch import nn
from torchvision.models import vgg19
from yacs.config import CfgNode

from . import ENCODER_REGISTRY


class VGG19FeatureExtractor(nn.Module):
    def __init__(self, i, j):
        """
        A truncated VGG19 network, such that its output is the 'feature map obtained by
        the j-th convolution (after activation) before the i-th maxpooling layer within
        the VGG19 network', as defined in the paper.
        Used to calculate the MSE loss in this VGG feature-space, i.e. the VGG loss.

        Args:
            i (int): MaxPooling layer index of VGG19 to truncate at.
            j (int): Conv layer index of VGG19 to truncate at.
        """

        super(VGG19FeatureExtractor, self).__init__()

        # Load the pre-trained VGG19 available in torchvision
        vgg19_model = vgg19(pretrained=True)

        maxpool_counter = 0
        conv_counter = 0
        truncate_at = 0
        # Iterate through the convolutional section ("features") of the VGG19
        for layer in vgg19_model.features.children():
            truncate_at += 1

            # Count the number of maxpool layers and the convolutional layers after each
            # maxpool
            if isinstance(layer, nn.Conv2d):
                conv_counter += 1
            if isinstance(layer, nn.MaxPool2d):
                maxpool_counter += 1
                conv_counter = 0

            # Break if we reach the jth convolution after the (i - 1)th maxpool
            if maxpool_counter == i - 1 and conv_counter == j:
                break

        # Check if conditions were satisfied
        assert (
            maxpool_counter == i - 1 and conv_counter == j
        ), "One or both of i=%d and j=%d are not valid choices for the VGG19!" % (i, j)

        # Truncate to the jth convolution (+ activation) before the ith maxpool layer
        self.truncated_vgg19 = nn.Sequential(
            *list(vgg19_model.features.children())[: truncate_at + 1]
        )

    def forward(self, input):
        return self.truncated_vgg19(input)


@ENCODER_REGISTRY.register("VGG19FeatureExtractor")
def build_vgg19featureextractor_encoder(encoder_cfg: CfgNode, **kwargs) -> nn.Module:
    """
    Helper function to build VGG19 Feature Extractor as described in SRGAN.

    Args:
        encoder_cfg (CfgNode): Config object from .yaml file.

    Returns:
        [nn.Module]: Initialized truncated VGG19.
    """

    model = VGG19FeatureExtractor(**kwargs)
    if encoder_cfg.get("pretrained_path"):
        pretrained_path = encoder_cfg["pretrained_path"]
        state_dict = torch.load(pretrained_path, map_location="cpu")
        model.load_state_dict(state_dict, strict=False)
    return model
