import torch.nn as nn

__all__ = ["ConvolutionalBlock", "ResidualBlock", "UpsamplingBlock"]


class ConvolutionalBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        stride=1,
        batch_norm=False,
        activation=None,
    ):
        super(ConvolutionalBlock, self).__init__()

        if activation is not None:
            activation = activation.lower()
            assert activation in {"prelu", "leakyrelu", "tanh"}

        # Container for layers of this convolutional block
        layers = list()

        # Add convolutional layer
        layers.append(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=kernel_size // 2,
            )
        )

        # Add batch normalisation layer if wanted
        if batch_norm is True:
            layers.append(nn.BatchNorm2d(out_channels))

        # Add activation layer
        if activation == "prelu":
            layers.append(nn.PReLU())
        elif activation == "leakyrelu":
            layers.append(nn.LeakyReLU(0.2))
        elif activation == "tanh":
            layers.append(nn.Tanh())

        # Create convolutional block as a sequence of layers
        self.conv_block = nn.Sequential(*layers)

    def forward(self, input):
        output = self.conv_block(input)
        return output


class ResidualBlock(nn.Module):
    def __init__(self, n_channels=64, kernel_size=3):
        super(ResidualBlock, self).__init__()

        self.conv1 = ConvolutionalBlock(
            in_channels=n_channels,
            out_channels=n_channels,
            kernel_size=kernel_size,
            batch_norm=True,
            activation="PReLU",
        )

        self.conv2 = ConvolutionalBlock(
            in_channels=n_channels,
            out_channels=n_channels,
            kernel_size=kernel_size,
            batch_norm=True,
            activation=None,
        )

    def forward(self, input):
        residual = input
        output = self.conv1(input)
        output = self.conv2(output)
        output = output + residual

        return output


class UpsamplingBlock(nn.Module):
    def __init__(self, n_channels=64, kernel_size=3, upscaling_factor=2):
        super(UpsamplingBlock, self).__init__()

        self.conv = ConvolutionalBlock(
            in_channels=n_channels,
            out_channels=n_channels * (upscaling_factor ** 2),
            kernel_size=kernel_size,
            batch_norm=False,
            activation=None,
        )
        self.pixel_shuffle = nn.PixelShuffle(upscale_factor=upscaling_factor)
        self.prelu = nn.PReLU()

    def forward(self, input):
        output = self.conv(input)
        output = self.pixel_shuffle(output)
        output = self.prelu(output)

        return output
