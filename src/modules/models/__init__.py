from .build import MODEL_REGISTRY, build_model_architecture
from src.modules.encoders.vgg19_feature_exractor import (
    build_vgg19featureextractor_encoder,
)

from .srgan import build_srresnet_model, build_srgan_model
