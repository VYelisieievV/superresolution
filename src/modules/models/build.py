from src.tools.registery import Registry
from torch import nn
from yacs.config import CfgNode

MODEL_REGISTRY = Registry()


def build_model_architecture(MODEL_cfg: CfgNode, *args, **kwargs) -> nn.Module:
    """
    Create model architecture with dynamic change of modules by changing config .yaml
    file.

    Args:
        MODEL_cfg (CfgNode): Config node of model.

    Returns:
        nn.Module: initialized model.
    """
    MODEL = MODEL_REGISTRY[MODEL_cfg.NAME](MODEL_cfg, *args, **kwargs)
    return MODEL
