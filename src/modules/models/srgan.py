from collections import OrderedDict

import pytorch_lightning as pl
import torch

from torch import nn
from yacs.config import CfgNode

from .build import MODEL_REGISTRY
from src.utils.cv_utils import convert_image
from src.modules.solver import build_optimizer
from src.modules.encoders import build_encoder


class SRResNet(pl.LightningModule):
    def __init__(
        self,
        generator_config,
        optimizer_config,
        n_channels=64,
        large_kernel_size=9,
        small_kernel_size=3,
        gen_blocks=16,
        scaling_factor=4,
    ):
        super().__init__()
        self.save_hyperparameters()
        self.optimizer_config = optimizer_config

        # network
        self.resnet = build_encoder(
            generator_config,
            n_channels,
            large_kernel_size,
            small_kernel_size,
            gen_blocks,
            scaling_factor,
        )

        self.loss_fn = nn.MSELoss()

        def forward(self, x):
            return self.resnet(x)

    def training_step(self, batch, batch_idx):
        lr_images, hr_images = batch

        # generate images through resnet and convert to hr_images type
        sr_images = self.resnet(lr_images)  # [-1, 1]
        sr_images = convert_image(sr_images, "[-1, 1]", "imagenet-norm")

        loss = self.loss_fn(sr_images, hr_images)
        return loss

    def configure_optimizers(self):
        return build_optimizer(self.resnet, self.optimizer_config)


class SRGAN(pl.LightningModule):
    def __init__(
        self,
        generator_config,
        discriminator_config,
        optimizers_config,
        vgg19_config,
        vgg19_i=5,  # the index i in the definition for VGG loss; see paper
        vgg19_j=4,  # the index j in the definition for VGG loss; see paper
        n_channels=64,
        large_kernel_size=9,
        small_kernel_size=3,
        gen_blocks=16,
        scaling_factor=4,
        disc_kernel_size=3,
        disc_blocks=8,
        full_size=1024,
        **kwargs,
    ):
        super().__init__()
        self.save_hyperparameters()
        self.optimizers_config = optimizers_config

        # networks
        self.generator = build_encoder(
            generator_config,
            n_channels=n_channels,
            large_kernel_size=large_kernel_size,
            small_kernel_size=small_kernel_size,
            gen_blocks=gen_blocks,
            scaling_factor=scaling_factor,
        )
        self.discriminator = build_encoder(
            discriminator_config,
            disc_kernel_size=disc_kernel_size,
            disc_blocks=disc_blocks,
            full_size=full_size,
        )

        self.feature_extractor = build_encoder(vgg19_config, i=vgg19_i, j=vgg19_j)

        self.content_loss_fn = nn.MSELoss()
        self.adversarial_loss_fn = nn.BCELoss()

    def forward(self, z):
        return self.generator(z)

    def training_step(self, batch, batch_idx, optimizer_idx):
        self.feature_extractor.eval()  # put vgg19 in eval for loss

        lr_images, hr_images = batch  # [0, 1], imagenet-norm
        # generate fake images through resnet and convert to hr_images type
        sr_images = self.generator(lr_images)  # [-1, 1]
        sr_images = convert_image(sr_images, "[-1, 1]", "imagenet-norm")

        # train generator
        if optimizer_idx == 1:
            # generate images
            sr_discriminated = self.discriminator(sr_images)

            # generator adversarial loss
            generator_adversarial_loss = self.adversarial_loss_fn(
                sr_discriminated, torch.ones_like(sr_discriminated)
            )

            # generator content loss
            with torch.no_grad():
                real_features = self.feature_extractor(hr_images)
                fake_features = self.feature_extractor(sr_images)

            generator_content_loss = self.content_loss_fn(
                sr_images, hr_images
            ) + 0.006 * self.content_loss_fn(fake_features, real_features.detach())

            # perceptual loss
            generator_loss = generator_content_loss + 0.001 * generator_adversarial_loss
            tqdm_dict = {"generator_loss": generator_loss}
            output = OrderedDict(
                {"loss": generator_loss, "progress_bar": tqdm_dict, "log": tqdm_dict}
            )
            return output

        # train discriminator
        if optimizer_idx == 0:
            # Measure discriminator's ability to classify real from generated samples
            hr_discriminated = self.discriminator(hr_images)
            sr_discriminated = self.discriminator(sr_images)

            # targets
            target_real = torch.ones_like(hr_discriminated)
            target_fake = torch.zeros_like(sr_discriminated)

            # discriminator loss
            real_loss = self.adversarial_loss_fn(hr_discriminated, target_real)
            fake_loss = self.adversarial_loss_fn(sr_discriminated, target_fake)
            discriminator_loss = real_loss + fake_loss

            tqdm_dict = {"discriminator_loss": discriminator_loss}
            output = OrderedDict(
                {
                    "loss": discriminator_loss,
                    "progress_bar": tqdm_dict,
                    "log": tqdm_dict,
                }
            )
            return output

    def configure_optimizers(self):
        opt_g = build_optimizer(self.generator, self.optimizers_config.GENERATOR_OPT)
        opt_d = build_optimizer(
            self.discriminator, self.optimizers_config.DISCRIMINATOR_OPT
        )
        return [opt_d, opt_g], []


@MODEL_REGISTRY.register("SRResNet")
def build_srresnet_model(model_cfg: CfgNode, **kwargs) -> nn.Module:
    """\
    Helper function to build SRResNet single image resolution model.

    Args:
        model_cfg (CfgNode): Config object from .yaml file.

    Returns:
        [nn.Module]: Initialized SRResNet.
    """

    model = SRResNet(**kwargs)

    if model_cfg.get("pretrained_path"):
        pretrained_path = model_cfg["pretrained_path"]
        state_dict = torch.load(pretrained_path, map_location="cpu")
        model.resnet.load_state_dict(state_dict, strict=False)

    return model


@MODEL_REGISTRY.register("SRGAN")
def build_srgan_model(model_cfg: CfgNode, **kwargs) -> nn.Module:
    """
    Helper function to build SRGAN single image resolution model.

    Args:
        model_cfg (CfgNode): Config object from .yaml file.

    Returns:
        [nn.Module]: Initialized SRGAN.
    """

    model = SRGAN(**kwargs)

    if model_cfg.get("pretrained_path_generator"):
        pretrained_path = model_cfg["pretrained_path_generator"]
        state_dict = torch.load(pretrained_path, map_location="cpu")
        model.generator.load_state_dict(state_dict, strict=False)

    if model_cfg.get("pretrained_path_discriminator"):
        pretrained_path = model_cfg["pretrained_path_discriminator"]
        state_dict = torch.load(pretrained_path, map_location="cpu")
        model.discriminator.load_state_dict(state_dict, strict=False)

    return model
