import torch
from torch import nn
from yacs.config import CfgNode


def build_loss(loss_cfg: CfgNode):
    raise NotImplementedError()
