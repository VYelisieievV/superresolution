import torch
from torch.optim.optimizer import Optimizer
from yacs.config import CfgNode


def build_optimizer(model: torch.nn.Module, opti_cfg: CfgNode) -> Optimizer:
    """
    simple optimizer builder
    :param model: already gpu pushed model
    :param opti_cfg: config node
    :return: the optimizer
    """
    parameters = model.parameters()
    opti_type = opti_cfg.NAME
    lr = opti_cfg.BASE_LR
    if opti_type == "adam":
        botas = opti_cfg.BETAS
        optimizer = torch.optim.Adam(parameters, lr=lr, betas=botas)
    elif opti_type == "sgd":
        sgd_cfg = opti_cfg.SGD
        momentum = sgd_cfg.MOMENTUM
        nesterov = sgd_cfg.NESTEROV
        optimizer = torch.optim.SGD(
            parameters, lr=lr, momentum=momentum, nesterov=nesterov
        )
    else:
        raise ValueError("invalid optimizer, available choices adam/sgd")
    return optimizer


def build_scheduler(optimizer: Optimizer, scheduler_cfg: CfgNode):
    """
    :param optimizer:
    :param optimizer: Optimizer
    :param scheduler_cfg:
    "param solver_cfg: CfgNode
    :return:
    """
    raise NotImplementedError()
