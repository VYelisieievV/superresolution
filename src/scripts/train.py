"""
train model
Usage:
    train.py -path_output=<path>  [--path_cfg_override=<path>]
    train.py -h | --help
Options:
    -h --help               show this screen help
    -path_output=<path>               output path
    --path_cfg_data=<path>       data config path [default: configs/data.yaml]
    --path_cfg_override=<path>            training config path
"""

from docopt import docopt
import os
from src.configs import get_cfg_defaults


if __name__ == "__main__":

    arguments = docopt(__doc__, argv=None, help=True, version=None, options_first=False)
    output_path = arguments["-path_output"]
    data_path = arguments["--path_cfg_data"]
    cfg_path = arguments["--path_cfg_override"]
    cfg = get_cfg_defaults()
    cfg.merge_from_file(cfg_path)
    if cfg_path is not None:
        cfg.merge_from_file(cfg_path)
    cfg.OUTPUT_PATH = output_path

    # Make output dir and its parents if they do not exist
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    # Make backup folders if they do not exist
    backup_dir = os.path.join(output_path, "model_backups")
    if not os.path.exists(backup_dir):
        os.mkdir(backup_dir)

    # Make result folders if they do not exist
    results_dir = os.path.join(output_path, "results")
    if not os.path.exists(results_dir):
        os.mkdir(results_dir)
