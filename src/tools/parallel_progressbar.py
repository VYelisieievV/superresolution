from tqdm.auto import tqdm
from joblib import Parallel
from typing import Union


class ProgressParallel(Parallel):
    def __init__(
        self, use_tqdm: bool = True, total: Union[None, int] = None, *args, **kwargs
    ):
        """Class that bridges tqdm progress bar functionality with parallel execution of joblib.

        Args:
            use_tqdm (bool, optional): Whether to use progress bar for parallel
                execution. Defaults to True.
            total (Union[None, int], optional): Total number of iterations. Defaults to
                None.
        """
        self._use_tqdm = use_tqdm
        self._total = total
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        with tqdm(disable=not self._use_tqdm, total=self._total) as self._pbar:
            return Parallel.__call__(self, *args, **kwargs)

    def print_progress(self):
        if self._total is None:
            self._pbar.total = self.n_dispatched_tasks
        self._pbar.n = self.n_completed_tasks
        self._pbar.refresh()
