# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.


class Registry(dict):
    """
    A helper class for managing registering `torch` modules, it extends a `dict` class
    and provides a register of functions.

    Eg. creating a registry:

    >>> some_registry = Registry({"default": default_module})

    There're two ways of registering new modules:

    1): normal way is just calling register function:
    >>> def foo():
    >>>     ...
    >>> some_registry.register("foo_module", foo)

    2): used as decorator when declaring the module:
    >>> @some_registry.register("foo_module")
    >>> @some_registry.register("foo_module_nickname")
    >>> def foo():
    >>>     ...

    Access of module is just like using a dictionary, eg:

    >>> f = some_registry["foo_module"]
    """

    def __init__(self, *args, **kwargs):
        super(Registry, self).__init__(*args, **kwargs)

    def _register_generic(self, module_name, module):
        assert module_name not in self
        self[module_name] = module

    # Decorator factory. Here self is a Registry dict
    def register(self, module_name, module=None):

        # Inner function used as function call
        if module is not None:
            self._register_generic(module_name, module)
            return

        # Inner function used as decorator -> takes a function as argument
        def register_fn(fn):
            self._register_generic(module_name, fn)
            return fn

        return register_fn  # decorator factory returns a decorator function
