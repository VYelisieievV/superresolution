import torch
from src.utils.torch_utils import get_default_device
from torchvision.transforms import functional as func_transforms

__all__ = ["IMAGENET_MEAN", "IMAGENET_STD", "convert_image"]

IMAGENET_MEAN = torch.Tensor([0.485, 0.456, 0.406]).unsqueeze(1).unsqueeze(2)
IMAGENET_STD = torch.Tensor([0.229, 0.224, 0.225]).unsqueeze(1).unsqueeze(2)


def convert_image(img, source_fmt, target_fmt):
    assert source_fmt in {
        "[0, 1]",
        "[-1, 1]",
        "pil",
        "cv2",
        "imagenet-norm",
    }, "Wrong source format."
    assert target_fmt in {
        "[0, 1]",
        "pil",
        "cv2",
        "imagenet-norm",
    }, "Wrong target format."

    # from source_fmt to [0, 1]
    if source_fmt == "[0, 1]":
        pass
    elif source_fmt == "[-1, 1]":
        img = (img + 1) / 2
    elif source_fmt == "pil":
        img = func_transforms.to_tensor(img)
    elif source_fmt == "cv2":
        img = torch.Tensor(img / 255)
    elif source_fmt == "imagenet-norm":
        if img.dim() == 3:
            img = img * IMAGENET_STD + IMAGENET_MEAN
        elif img.dim() == 4:
            imagenet_mean_cuda = IMAGENET_MEAN.type_as(img)
            imagenet_std_cuda = IMAGENET_STD.type_as(img)
            img = img * imagenet_std_cuda + imagenet_mean_cuda

    # from [0. 1] to target_fmt
    if target_fmt == "[0, 1]":
        pass
    elif target_fmt == "pil":
        img = func_transforms.to_pil_image(img)
    elif target_fmt == "cv2":
        img = img * 255
    elif target_fmt == "imagenet-norm":
        if img.dim() == 3:
            img = (img - IMAGENET_MEAN) / IMAGENET_STD
        elif img.dim() == 4:
            imagenet_mean_cuda = IMAGENET_MEAN.type_as(img)
            imagenet_std_cuda = IMAGENET_STD.type_as(img)
            img = (img - imagenet_mean_cuda) / imagenet_std_cuda

    return img
